# StoreGateway

This application was generated using JHipster, you can find documentation and help at [https://jhipster.github.io](https://jhipster.github.io).

~~~
                                                   /-------------\
                                                   |   Browser   |
                                                   \------+------/
                                                          |
                                                          |
                                                          V 8080
                               /-----------------------------------\
                               | Gateway                           |
                               | +-----------+    +--------------+ |
                               | |           |    |  Zuul Proxy  | |
                               | | Store     |    |              | |
                               | | Gateway   |    +--------------+ |
                               | |           |    |    Ribbon    | |
                               | +-----------+    +---+------+---+ |
                               |                      |      |     |
                               \-+---------------------------------/
                                 |                    |      |       
                                 |                    |      |                              
                                 |                    |      |                        
                                 |        +-----------/      \----------------+            
/-----------------------\        |        |                                   |
| JHipster Registry     |        |        V 8081                              V 8082
|                       |        | /----------------\     +-----+         /-----------------------------\
|  +-----------------+  |        | |                |     |     |         |                             |
|  | Eureka Server   |  |        | |  ShoppingCart  |---->| DB  |         |  Slots to add services....  |
|  +-----------------+  |        | |                |     |     |         |                             |
|  | Config Server   |  |        | \--+-------------/     +-----+         +--+--------------------------/
|  +--------+--------+  |  8761  |    |                                      |
|           |           |<-------+----+--------------------------------------/      
\-----------------------/                                                      
         
         
~~~


## Development

Before you can build this project, you must install and configure the following dependencies on your machine:

1. [Node.js][]: We use Node to run a development web server and build the project.
   Depending on your system, you can install Node either from source or as a pre-packaged bundle.

After installing Node, you should be able to run the following command to install development tools (like
[Bower][] and [BrowserSync][]). You will only need to run this command when dependencies change in package.json.

    npm install

We use [Gulp][] as our build system. Install the Gulp command-line tool globally with:

    npm install -g gulp

Run the following commands in two separate terminals to create a blissful development experience where your browser
auto-refreshes when files change on your hard drive.

    ./mvnw
    gulp

Bower is used to manage CSS and JavaScript dependencies used in this application. You can upgrade dependencies by
specifying a newer version in `bower.json`. You can also run `bower update` and `bower install` to manage dependencies.
Add the `-h` flag on any command to see how you can use it. For example, `bower update -h`.


## Building

    ./mvnw -package

## Running

    java -jar target/*.war

Then navigate to [http://localhost:8080](http://localhost:8080) in your browser.


[Node.js]: https://nodejs.org/
[Bower]: http://bower.io/
[Gulp]: http://gulpjs.com/
[BrowserSync]: http://www.browsersync.io/
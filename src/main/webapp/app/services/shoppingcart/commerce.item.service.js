(function() {
    'use strict';

    angular
        .module('storegatewayApp')
        .factory('CommerceItemService', CommerceItemService);

    CommerceItemService.$inject = ['$resource'];

    function CommerceItemService ($resource) {
        var service = $resource('shoppingcart/api/shoppingcart/items/:id', {'product_id':'@productId','quantity':'@quantity'}, {
            'add': { method:'POST' },
            'delete':{ method:'DELETE'}
        });

        return service;
    }
})();

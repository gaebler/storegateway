(function() {
    'use strict';

    angular
        .module('storegatewayApp')
        .factory('ProductService', ProductService);

    ProductService.$inject = ['$resource'];

    function ProductService ($resource) {
        var service = $resource('shoppingcart/api/products', {}, {
            'get': {
                method: 'GET',
                isArray: true
            }
        });

        return service;
    }
})();

(function() {
    'use strict';

    angular
        .module('storegatewayApp')
        .factory('ShoppingCartService', ShoppingCartService);

    ShoppingCartService.$inject = ['$resource'];

    function ShoppingCartService ($resource) {
        var service = $resource('shoppingcart/api/shoppingcart', {}, {
            'get': {
                method: 'GET',
                isArray: false
            }
        });

        return service;
    }
})();

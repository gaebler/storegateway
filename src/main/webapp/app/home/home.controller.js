(function() {
    'use strict';

    angular
        .module('storegatewayApp')
        .controller('HomeController', HomeController);

    HomeController.$inject = ['$scope', '$state', 'ProductService', 'ShoppingCartService', 'CommerceItemService'];

    function HomeController ($scope, $state, ProductService, ShoppingCartService, CommerceItemService) {
        var vm = this;

        vm.products = [];
        vm.productsDescriptions = {};
        vm.shoppingcart = {};
        vm.loadPage = loadPage;
        vm.addItem = addItem;
        vm.deleteItem = deleteItem;

        vm.loadPage();

        function loadPage () {
            loadProducts();
            loadShoppingCart();
        }

        function loadProducts () {
            ProductService.get(function(result) {
                vm.products = result;
                vm.productsDescriptions = vm.products.reduce(function(map, obj) {
                                                map[obj.id] = obj.name;
                                                return map;
                                            }, {});
            });
        }

        function loadShoppingCart () {
            ShoppingCartService.get(function(result) {
                vm.shoppingcart = result;
            });
        }

        function addItem (productId, quantity) {
            var item = {};
            item.productId = productId;
            item.quantity = quantity;
            CommerceItemService.add(item, function(result) {
                loadShoppingCart();
            });
        }

        function deleteItem (itemId) {
            CommerceItemService.delete({id: itemId}, function(result) {
                loadShoppingCart();
            });
        }
    }
})();

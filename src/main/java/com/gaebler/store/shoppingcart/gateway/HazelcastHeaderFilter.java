package com.gaebler.store.shoppingcart.gateway;

import org.springframework.stereotype.Component;

import com.netflix.zuul.ZuulFilter;
import com.netflix.zuul.context.RequestContext;

@Component
public class HazelcastHeaderFilter extends ZuulFilter {
	
	@Override
    public Object run() {
        RequestContext ctx = RequestContext.getCurrentContext();
        RequestContext.getCurrentContext().addZuulRequestHeader("hazelcast.sessionId",ctx.getRequest().getSession().getId());
        return null;
    }

    @Override
    public boolean shouldFilter() {
        return true;
    }

    @Override
    public String filterType() {
        return "pre";
    }

    @Override
    public int filterOrder() {
        return 10000;
    }
}
